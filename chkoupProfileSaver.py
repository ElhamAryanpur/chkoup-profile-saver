from base64 import urlsafe_b64encode, urlsafe_b64decode
from os import getcwd
import sqlite3


class ChkoupProfileSaver:
    """
        Chkoup Profile Saver class containing all methods for saving profile data

        Params:
        1. database_location: String | Path for database to save
    """

    def __init__(self, database_location):
        if type(database_location) == str and len(database_location) > 0:
            try:
                self.db = sqlite3.connect(database_location)
            except Exception as e:
                print("Error while creating the database: {}".format(e))
        else:
            print("Location Doesn't Exist, Creating The Database In Current Directory!")
            try:
                self.db = sqlite3.connect("{}.db".format(getcwd()))
            except Exception as e:
                print("Error while creating the database: {}".format(e))

        self.db_cursor = self.db.cursor()

    def table_new(self, table_name, fields):
        """
            Creates a new table

            Params:
            1. table_name: String | Name of the table
            2. fields: List | List of the fields for the table
        """

        if type(table_name) == str and type(fields) == list:
            try:
                # encoding (compressing) name of table
                table_name = self._encode(table_name)

                # making this way so that each field name be encoded too
                field_command = ""
                for i in range(len(fields)):
                    field_command += " {} {},".format(
                        self._encode(fields[i]["name"]), fields[i]["type"])
                field_command = field_command[0:len(field_command) - 1]

                # and join them in one full command.
                # this helps with sql injections!
                command = "CREATE TABLE IF NOT EXISTS {} (ID INTEGER PRIMARY KEY,{})".format(
                    table_name, field_command)

                # executing the command and saving
                self.db_cursor.execute(command)
                self.db.commit()
                return True

            except Exception as e:
                print("Error while creating table: {}".format(e))
                return False
        else:
            return False

    def table_drop(self, table_name):
        """
            Deletes a table if exists

            Params:
            1. table_name: String | Name of the table
        """

        if type(table_name) == str and len(table_name) > 0:
            # encode table name
            table_name = self._encode(table_name)

            self.db_cursor.execute("DROP TABLE {}".format(table_name))
            self.db.commit()
            return True
        else:
            print("Couldn't drop the table {}".format(table_name))
            return False

    def insert(self, table_name, fields, query):
        """
            Inserts a row into the table

            Params:
            1. table_name: String | Name of the table
            2. fields: List | List of the fields for the table
            3. query: Tuple | A tuple that represent row values
        """

        if type(table_name) == str and len(table_name) > 0 and type(fields) == list and len(fields) > 0 and type(query) == tuple and len(query) > 0:
            table_name = self._encode(table_name)

            field_command = ""
            for i in range(len(fields)):
                field_command += "{}, ".format(
                    self._encode(fields[i]["name"]))
            field_command = field_command[0:len(field_command) - 2]

            query_command = ""
            for i in range(len(query)):
                query_command += "?, "
            query_command = query_command[0:len(query_command) - 2]

            command = "INSERT INTO {}({}) VALUES({})".format(
                table_name, field_command, query_command)
            self.db_cursor.execute(command, query)
            self.db.commit()
            return True
        else:
            print("Invalid table name, or fields, or query format!")
            return False

    def get_all(self, table_name, callback=None):
        """
            Retrieves all rows of the table

            Params:
            1. table_name: String | Name of the table
            2. callback: Function | A function that will retrieve the result as first argument. If no callback is specified, the result is returned.
        """

        if type(table_name) == str and len(table_name) > 0:
            table_name = self._encode(table_name)
            self.db_cursor.execute("SELECT * FROM {}".format(table_name))
            if callback:
                callback(self.db_cursor.fetchall())
                return True
            else:
                return self.db_cursor.fetchall()
        else:
            print("Invalid table name!")
            return False

    def get_custom(self, table_name, query, callback=None):
        """
            Retrieves rows of table according to custom query

            Params:
            1. table_name: String | Name of the table
            2. query: String | An SQL command specifying the conditions for the data search
            3. callback: Function | A function that will retrieve the result as first argument. If no callback is specified, the result is returned.
        """

        if type(table_name) == str and len(table_name) > 0 and type(query) == str and len(query) > 0:
            table_name = self._encode(table_name)
            self.db_cursor.execute(
                "SELECT * FROM {} WHERE {}".format(table_name, query))
            if callback:
                callback(self.db_cursor.fetchall())
                return True
            else:
                return self.db_cursor.fetchall()
        else:
            print("Invalid table name or query!")
            return False

    def search(self, table_name, key, value, callback=None):
        """
            Searches the table for similar rows! if the value is text, use % before or after the value to autofill.

            Params:
            1. table_name: String | Name of the table
            2. key: String | The key to search on
            3. value: Any | The value to search similar of
            4. callback: Function | A function that will retrieve the result as first argument. If no callback is specified, the result is returned.
        """

        if type(table_name) == str and len(table_name) > 0 and type(key) == str and len(key) > 0:
            table_name = self._encode(table_name)
            key = self._encode(key)
            self.db_cursor.execute(
                "SELECT * FROM {} WHERE {} LIKE ? COLLATE NOCASE".format(table_name, key), (value,))
            if callback:
                callback(self.db_cursor.fetchall())
                return True
            else:
                return self.db_cursor.fetchall()
        else:
            print("Invalid table name, key or value!")
            return False

    def update(self, table_name, id, fields, new_values):
        """
            Changes data of specific row

            Params:
            1. table_name: String | Name of the table
            2. id: Int | The ID of the field to change
            3. fields: List | The list of fields that the value will change
            4. new_values: Tuple | The new data that will replace old one. should be in order of fields specified
        """

        if type(table_name) == str and len(table_name) > 0 and type(id) == int and type(fields) == list and type(new_values) == tuple:
            table_name = self._encode(table_name)

            try:
                command = "UPDATE {} SET ".format(table_name)
                for i in range(len(fields)):
                    command += "{}=?, ".format(self._encode(fields[i]))
                command = command[0:len(command) - 2] + \
                    " WHERE ID={}".format(id)
                self.db_cursor.execute(command, new_values)
                self.db.commit()
                return True
            except Exception as e:
                print("Error while updating table with new data...:{}".format(e))
                return False
        else:
            print("Invalid table name, id, field, or new value format!")
            return False

    def delete(self, table_name, key, value):
        """
            Deletes a(ll) row(s) as specified

            Params:
            1. table_name: String | Name of the table
            2. key: String | The field to search and delete
            3. value: Any | The matching values of the rows to delete
        """

        if type(table_name) == str and len(table_name) > 0 and type(key) == str and len(key) > 0:
            table_name = self._encode(table_name)
            key = self._encode(key)
            try:
                self.db_cursor.execute(
                    "DELETE FROM {} WHERE {} = ? COLLATE NOCASE".format(table_name, key), (value,))
                self.db.commit()
                return True
            except Exception as e:
                print("Error while removing from the table...: {}".format(e))
                return False
        else:
            print("Invalid table name or query!")
            return False

    def custom(self, query):
        if type(query) == str and len(query) > 0:
            try:
                self.db_cursor.execute(query)
                self.db.commit()
                return True
            except Exception as e:
                print("Couldn't run the query...: {}".format(e))
                return False
        else:
            print("Invalid query or query type!")
            return False

    def count(self, table_name, callback=None):
        """
            Counts the number of valid rows in the table.

            Params:
            1. table_name: String | Name of the table
            2. callback: Function | A function that will retrieve the result as first argument. If no callback is specified, the result is returned.
        """

        if type(table_name) == str and len(table_name) > 0:
            table_name = self._encode(table_name)

            self.db_cursor.execute(
                "SELECT COUNT(ID) FROM {}".format(table_name))
            if callback:
                callback(self.db_cursor.fetchall()[0][0])
                return True
            else:
                return self.db_cursor.fetchall()
        else:
            print("Invalid table name!")
            return False

    def biggest(self, table_name, key, callback=None):
        """
            Retrieves all highest values from a table

            Params:
            1. table_name: String | Name of the table
            2. callback: Function | A function that will retrieve the result as first argument. If no callback is specified, the result is returned.
        """

        if type(table_name) == str and len(table_name) > 0 and type(key) == str and len(key) > 0:
            table_name = self._encode(table_name)
            key = self._encode(key)
            self.db_cursor.execute(
                "SELECT * FROM {} ORDER BY {} DESC".format(table_name, key))
            if callback:
                callback(self.db_cursor.fetchall())
                return True
            else:
                return self.db_cursor.fetchall()
        else:
            print("Invalid table name or key!")
            return False

    def close(self):
        """
            Closes the database
        """
        self.db.close()
        return True

    def _encode(self, data):
        """
            Encodes the data into base64

            Params:
            1. data: String | The data to encode
        """

        if type(data) == str and len(data) > 0:
            try:
                return urlsafe_b64encode(data.encode("ascii")).decode("ascii").strip("=")
            except Exception as e:
                print("Error while encoding data: {}".format(e))
                return None
        else:
            print("Can't encode {} because it's not string...".format(data))
            return None

    def _decode(self, data):
        """
            Decodes the data into original form

            Params:
            1. data: String | The data to decode
        """

        if type(data) == str and len(data) > 0:
            try:
                padding = 4 - (len(data) % 4)
                data = data + ("=" * padding)
                return urlsafe_b64decode(data.encode("ascii")).decode("ascii")
            except Exception as e:
                print("Error while decoding data: {}".format(e))
                return None
        else:
            print("Can't decode {} because it's not encoded data...".format(data))
            return None


if __name__ == "__main__":
    print("Starting the Chkoup Profile Saver script test!")
    print("\nCreating fields for test and database on memory\n")
    db = ChkoupProfileSaver(":memory:")
    fields = [
        {"name": "name", "type": "TEXT"},
        {"name": "number", "type": "INT"}
    ]
    print("\nCreating a table by name 'test' with fields specified\n")
    if db.table_new("test", fields):
        print("Table creation success!\n")
    else:
        print("Table creation failed :( \n")

    print("\nInserting some values to the table\n")
    if db.insert("test", fields, ("Elham", 1000,)):
        print("Inserting value success!\n")
    else:
        print("Inserting value failed :( \n")

    print("\nInserting some values to the table\n")
    if db.insert("test", fields, ("Sami", 2000,)):
        print("Inserting value success!\n")
    else:
        print("Inserting value failed :( \n")

    print("\nInserting some values to the table\n")
    if db.insert("test", fields, ("Shafiq", 3000,)):
        print("Inserting value success!\n")
    else:
        print("Inserting value failed :( \n")

    print("\nRetrieve all values in the table\n")
    db.get_all("test", print)

    print("\nRetrieve only one specified value\n")
    db.search("test", "name", "elh%", print)

    print("\nUpdating the third row's name to Yama\n")
    if db.update("test", 3, ["name"], ("Yama",)):
        print("Updating value success!\n")
    else:
        print("Updating value failed :( \n")

    print("Retrieve the changed value\n")
    db.search("test", "name", "ya%", print)

    print("\n\nDeleting a row\n")
    if db.delete("test", "name", "Elyas"):
        print("Deleting row success!\n")
    else:
        print("Deleting row failed :( \n")

    print("\nCount the number of rows\n")
    db.count("test", print)

    print("\nGet the data order by largest\n")
    db.biggest("test", "number", print)

    print("\nAll tests done, deleting table, closing the database! Hope you Enjoy!\n")
    db.table_drop("test")
    db.close()
