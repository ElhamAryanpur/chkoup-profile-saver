# Chkoup's Bot Profile Saver

## About

A database script for Chkoup's bot for saving and accessing profile data with max performance and least resource use.

## Dependencies

The script use purely standard library of win64.

## Docs

`NOTE`: The script is under development so not ready for production use yet!

### Creation

ChkoupProfileSaver uses sqlite for backend. To create a database, you can either save it on disk or on memory. The database creation is done as you assign the class.

```python
on_disk = ChkoupProfileSaver("/path/to/save.extension")

# Database on memory is made by passing exactly :memory: as argument
on_memory = ChkoupProfileSaver(":memory:")
```

### Tables

Table creation and deletion is supported. For creating a table, you'll need field configuration too! A field configuration is a list of dictionaries that contain two values: `name` and `type`.

Then each `name` will be encoded with url-safe base64 for encryption purposes!

The `type`s define which datatype will the field be. All of Sqlite's fields are supported!

All new tables will be equipped with `ID` field as auto increment integer for values.

```python
my_fields = [
    {"name": "text field", "type": "TEXT"},
    {"name": "number field", "type": "INT"},
    # and other fields as you wish
]
# Do remember to keep your fields on separate variable and on a place accessable by your other database operations. Also keep in mind to save them somewhere if you make fields dynamically to prevent data loss as you'll need field structure often!

# now create a new table using `table_new` method
result = database_variable.table_new("my table name", my_fields)

# All methods return a value which except the data retrieval ones, the return value will be always a boolean indicating the status of the operation. A message showing which part gone wrong will be displayed too if you get a False return!
if result:
    print("Creating table was a success!")
else:
    print("Couldn't create the table :( ")

# and to delete a table
database_variable.table_drop("my table name")
```

### Data inserting, deleting and updating

Inserting and updating data is straight forward!

```python
# to insert data use `insert` method and, you should pass the new data as tuple!
# Be sure to have all new data according to field structure order!
database_variable.insert("my table name", my_fields, ("value1", 123,))

# Updating the data require you to have the ID of the data. This is for preventing multiple data being changed unwantedly
# You also pass the fields you want to change, as list, and new data in order of fields, as tuple.
database_variable.update("my table name", 1, ["field 1"], ("new value",))

# for deletion, you need a field to match a certain value so that it finds and deletes. NOTE: this can delete multiple rows!
database_variable.delete("my table name", "field 1", "value to delete row of")

# also be sure to check the returns for error validation
```

### Data retrieval

There are 3 ways to retrieve data. You can also assign a callback function with one argument for result value, or it'll just return it.

```python
# First option is, like gladiator, get all values! let's get return value too!
result = database_variable.get_all("my table name")
if result != False:
    print(result)
else:
    print("Couldn't get all the values :( ")

# Second option is to search for a specific value!
# if your field is text, you can use % sign before or after text to autofill it! also it's not case sensitive.
database_variable.search("my table name", "field to search", "value you're looking for with autofill%", print)

# Third option is custom query for retrieval. You can use a custom SQL query. Do remember all field names are in URL safe base64 encoding, you can encode yours by using the 
desired_field = database_variable._encode("your field")
database_variable.get_custom("my table name", "{} = 'value'".format(desired_field), print)
```

### Utility functions

These are helper functions that does simple things that might make your life easier!

#### Count

Returns or callback the number of rows in a table

```python
database_variable.count("my table name", print)
```

#### Biggest

Returns or callback all rows ordered by specific field's biggest to smallest. Is good for leaderboards for example.

```python
database_variable.biggest("my table name", "points field", print)
```

#### Custom

Runs a custom query on the database. `WARNING`: It might cause destruction of data if not carefully handled!

```python
database_variable.custom("CREATE TABLE custom (field1 TEXT)")
```

## Footer

The script uses standard library, so it should be able to run anywhere the standard library can run!

This script is made for chkoup, MIT licensed. Hope it helps!
